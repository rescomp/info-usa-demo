# InfoUSA Dataset Merge Tool

This tool allows you to merge and subset files in the InfoUSA dataset according to your research needs. You can perform spatial subsetting of the data (e.g., selecting files from a specific country or state) or subsetting based on a list of zip codes provided by the researcher.

## Requirements
- Access to the InfoUSA dataset on your system
- Shapefiles for spatial subsetting (optional)

## Usage 
To run the script, use the following command:
```
sbatch --export=ALL,zipcode_in=<path/to/your/data.txt>,infousa_in=/cifs/infousa,output_in=<path/to/save/your/data>,spatial_in=<path/to/your/data.shp_or_NA>,zipcode_path_in=<path/to/your/shapefile.shp>,output_filename=<output_filename>,year=<year>,jobid=<jobid>,netid=<netid> merge_per_year.sh
```

Replace the placeholders with the appropriate values:

- `<path/to/your/data.txt>`: Path to the text file containing the list of zip codes (one per line) you want to subset the data based on.
-` /cifs/infousa`: Path to the InfoUSA dataset on your system.
- `<path/to/save/your/data>`: Path where you want to save the output file.
- `<path/to/your/data.shp_or_NA>`: Path to the spatial data (shapefile) you want to subset the data based on. Use NA if you're using a zip code list instead.
- `<path/to/your/shapefile.shp>`: Path to the shapefile for spatial subsetting.
- `<output_filename>`: Name of the output file.
- `<year>`: Year of the data you want to process (e.g., 2021).
- `<jobid>`: Job ID for the sbatch job.
- `<netid>`: Your network ID.

## Example
```
sbatch --export=ALL,zipcode_in=/data/zipcode_list.txt,infousa_in=/cifs/infousa,output_in=/output/data,spatial_in=NA,zipcode_path_in=/shapes/zipcode_shapefile.shp,output_filename=merged_data,year=2021,jobid=1,netid=xc184 merge_per_year.sh
```

This command will process the 2021 InfoUSA dataset, subset the data based on the list of zip codes in `/data/zipcode_list.txt`, and save the output to `/output/data/merged_data`.
## Notes

- Use either a list of zip codes or a shapefile for subsetting, not both.
- Ensure that the paths to the files and directories are correct.
- The merge script (`merge_per_year.sh`) should be in the same directory where you run the sbatch command.
