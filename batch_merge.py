import argparse
import pandas as pd
import geopandas as gpd
import os
import warnings
import shutil
warnings.filterwarnings('ignore')

def merge_files_textfile(zipcode_file, infousa_path, output_path, filename, year, jobid, netid):
    """
    Takes in all the zipcodes in zipcode_file and merges the zipcode files in the InfoUSA directory with those
    zipcodes, writing them to a single, merged parquet file in the output directory.
    Args:
        zipcode_file (str): a textfile with one zipcode per line
        infousa_path (str): a complete path to the InfoUSA directory
        output_path (str): a complete path to a folder (with writing permissions) for the merged files to be written into
        filename (str): the name of the file which contains the zipcodes we are merging
    """
    infousa_path = infousa_path + r"/derived set/" + str(year)
    print("infousa path is: ", infousa_path)
    
    if not os.path.exists(infousa_path):
        print("The InfoUSA directory path you entered does not exist.")
        return
    
    os.chdir(infousa_path)
    infousa_directory = os.listdir(infousa_path)
    
    # reading in the input text file and formatting file names
    # to match how they are formatted in the InfoUSA directory
    with open(zipcode_file) as f:
        zipcode_list = ["Household_Ethnicity_zip_" + line.rstrip('\n') + "_year_" + str(year) + ".txt" for line in f]
    
    # only keeping the file names for zipcodes which are in the InfoUSA dataset
    files = set(infousa_directory).intersection(set(zipcode_list))
    
    df_merged = pd.concat([pd.read_csv(f, sep = '\t', dtype = str) for f in files])
        
    # if there is already not a folder for the merged InfoUSA files in the 
    # output directory, make one  
    
    if not os.path.isdir(output_path + r'/merged_infousa_files_' + str(year) + '-' + str(jobid) + '-' + str(netid)):
        os.mkdir(output_path + r'/merged_infousa_files_' + str(year) + '-' + str(jobid) + '-' + str(netid))
        
    # print("output path is: " + output_path + r'/merged_infousa_files_' + str(year))

    # write the merged dataframe into a subfolder in the output directory
    # as a parquet file
    print("exporting " + str(output_path) + r'/merged_infousa_files_' + str(year) + r'/' + str(filename)[-20:-4] + '.parquet')
    df_merged.to_parquet(str(output_path) + r'/merged_infousa_files_' + str(year) + '-' + str(jobid) + '-' + str(netid) + r'/' + str(filename)[-20:-4] + '.parquet', index = False)
    print("exported")
    
    # after all files are merged, delete split_files folder
    # shutil.rmtree(str(output_path) + '/split_files_' + str(year) + "-" + str(jobid) + "-" + str(netid))

def get_args_parse():
    
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Options for InfoUSA dataset subsetting')

    # Add the arguments
    my_parser.add_argument('--infousa', type=str, required=True, 
                           help='a complete path to the InfoUSA dataset')
    
    my_parser.add_argument('--output', type=str, required=True, 
                           help='a complete path to the folder you\'d like your output to be written to')

    my_parser.add_argument('--zipcode_input', type=str, required=True, 
                           help='a complete path to a .txt file with the zipcodes to subset by')

    my_parser.add_argument('--filename', type=str, required=True,
                           help='file name')
    
    my_parser.add_argument('--year', type=str, required=True,
                           help='year')

    my_parser.add_argument('--jobid', type=str, required=True,
                       help='JobID')
    
    my_parser.add_argument('--netid', type=str, required=True,
                       help='User NetID')
    
    # Execute the parse_args() method
    args = my_parser.parse_args()
    
    return args

if __name__ == '__main__':
    
    args = get_args_parse()
          
    if args.zipcode_input:        
        zipcode_file = get_args_parse().zipcode_input
        infousa_path = get_args_parse().infousa
        output_path = get_args_parse().output
        filename = get_args_parse().filename
        year = get_args_parse().year
        jobid = get_args_parse().jobid
        netid = get_args_parse().netid
        
        merge_files_textfile(zipcode_file, infousa_path, output_path, filename, year, jobid, netid)
                
    else:
        print('No zipcode file path entered')