# Efficiently Processing and Merging InfoUSA files

### Author: Alyssa Ting
### Mentors: Celine Robinson, Mark McCahill

## Project Introduction 
The primary goal of this project was to develop a series of sbatch scripts which efficiently processes/merge files in the InfoUSA dataset according to researcher needs. This includes offereing options for spatial subsetting of the data (looking to only pull files from a specific country or state) or subsetting based on a list of zipcodes provided by the researcher.

## About the Data
The InfoUSA dataset contains household-level demographic data, with variables spanning household coordinates, average income, number of children per household, and more. The InfoUSA directory spans multiple different years, and for each year, contains 1 file per zipcode in the US. This dataset can be used to enrich researchers' ability to incorporate demographic data into their work, however it can be time-consuming to process.

## Description of Included Files:
This repository contains an sbatch script that calls the Python scripts appropriately in order to perform a merge based on spatial subset of the data or a given textfile of zipcodes.

### ```year_subset.sh```
```year_subset.sh``` is an sbatch script which calls ```merge_per_year.sh```, another sbatch script, which merges the desired InfoUSA data for each year in the text file provided.

### ```merge_per_year.sh```
```merge_per_year.sh``` is an sbatch script which calls the Python scripts provided in the file to efficiently merge InfoUSA files based on the researcher's needs (either a spatial subset or a subset based on a provided textfile of zipcodes to pull data for).

* **Spatial subset:** If the researcher would like a spatial subset, this sbatch script will first call Python script ```spatial_subset.py```, which will output (into the output directory) a textfile with the zipcodes that lie within the geometries provided. Then, the script will call ```split.py``` to split the aforementioned zipcode textfile into 1,000 file chunks, and call ```batch_merge.sh``` to execute ```batch_merge.py``` on each of these textfiles. Lastly, the sbatch script will call ```concat.sh``` to execute ```concat.py``` and merge the separate ```.parquet``` files for each chunk into one, final, merged dataframe, and write it out to the output directory as a ```.parquet``` file.
* **Merge based on a provided textfile:** This operates almost exactly like the above. If the researcher would like to pull data for zipcodes specified in a textfile (with one zipcode per line), this sbatch script will skip the spatial subsetting part described above and start by splitting the input zipcode textfile into 1,000 file chunks. It will then continue exactly the same as the processes above, until there is a final, merged dataframe written to the output directory as a ```.parquet``` file.

### ```spatial_subset.py```
```spatial_subset.py``` is a Python script which uses flags to take in four command-line arguments:
* **--spatial_file:** this flag takes in the spatial_file argument for the script. It must be a complete path to a shapefile containing the geometries the researcher would like to subset by. For example, if the researcher would like to pull InfoUSA data for New York, they would need to provide a shapefile containing ONLY geometries for New York.
* **--zipcode_file:** this flag takes in the zipcode_file argument for the script. It must be a complete path to the shapefile containing the geometries of all zipcodes in the US. Such a file can be found [here](https://www.census.gov/geographies/mapping-files/time-series/geo/carto-boundary-file.html).
* **--infousa_path:** this flag takes in the infousa_path argument for the script. It must be a complete path to the directory containing the InfoUSA files the researcher would like to merge. If they would like to merge files from a specific year, this path must be to that specific year; i.e.: /infousa/2020 instead of simply /infousa.
* **--output_path:** this flag takes in the output_path argument for the script. It must be a complete path to the directory folder the researcher would like to output the merged files to.
* **--year:** this flag in takes the year of InfoUSA data currently being subsetted.

The script will read in the shapefiles provided by the ```spatial_file``` and ```zipcode_file``` arguments. Then, it will use the GeoPandas library's ```sjoin()``` function to create a dataframe only containing geometries in the subsetting spatial file which intersect with the geometries in the zipcode shapefile provided. In other words, it will create a dataframe containing geometries for only the zipcodes within the regions provided by the ```spatial_file``` argument. Then, it will pull the zipcodes from this data frame into a list, and from this list create a textfile with each zipcode per line. This textfile will be written out to the output directory given in ```output_path```.

### ```split.py```
```split.py``` is a Python script which uses flags to take in two command-line arguments:
* **--output_path:** this flag takes in the output_path argument for the script. It must be a complete path to the directory folder the researcher would like to output the merged files to.
* **--zipcode_input:** this flag takes in the zipcode_input argument for the script. It must be a complete path to the textfile containing the zipcodes to merge the InfoUSA files by. For example, if a researcher would like InfoUSA data on zipcodes 93401, 87620 and 762217, they would make a textfile with one zipcode per line and use the path to that textfile as the argument for this flag.
* **--year:** this flag in takes the year of InfoUSA data currently being subsetted.

The script will take the textfile provided by the ```zipcode_input``` argument and split it into 1,000 zipcode chunks. Each new textfile will have at MOST 1,000 lines, each line one zipcode the researcher would like data from. Each textfile created will be written to a folder named ```split_files_[year]``` in the output directory, where the year comes from input argument ```year```.

### ```batch_merge.py```
```batch_merge.py``` is a Python script which uses flags to take in four command-line arguments: 
* **--zipcode_input:** this flag takes in the zipcode_input argument for the script. It must be a complete path to the textfile containing the zipcodes to merge the InfoUSA files by. For example, if a researcher would like InfoUSA data on zipcodes 93401, 87620 and 762217, they would make a textfile with one zipcode per line and use the path to that textfile as the argument for this flag.
* **--infousa_path:** this flag takes in the infousa_path argument for the script. It must be a complete path to the directory containing the InfoUSA files the researcher would like to merge. If they would like to merge files from a specific year, this path must be to that specific year; i.e.: /infousa/2020 instead of simply /infousa.
* **--output_path:** this flag takes in the output_path argument for the script. It must be a complete path to the directory folder the researcher would like to output the merged files to.
* **--filename**: this flag takes in the filename argument for the script. It ensures that when ```batch_merge.py``` is called for every 1,000-file chunk, the merged file of those 1000 zipcodes is saved with the appropriate name.
* **--year:** this flag in takes the year of InfoUSA data currently being subsetted.

The script will read in the textfile provided by the ```--zipcode_input``` argument and create a list of zipcodes. Then, it will filter for only zipcodes that are provided in the InfoUSA dataset, and iterate through each zipcode in that list, merging the InfoUSA file for that zipcode, until there is one dataframe which is the merged files for that 1,000 zipcode chunk. The script will then write the merged dataframe to the output directory as a ```.parquet``` file.

### ```concat.py```
```concat.py``` is a Python script which uses flags to take in one command-line argument:
* **--output_path:** this flag takes in the output_path argument for the script. It must be a complete path to the directory folder the researcher would like to output the merged files to.
* **--year:** this flag in takes the year of InfoUSA data currently being subsetted.

The script will look in the output directory for the ```.parquet``` files containing each chunk of 1,000 merged zipcodes, and merge all those files into one, final, merged dataframe. This dataframe will be written to the ```merged_infousa_files_[year]``` folder within the output directory, where the year comes from input argument ```year```.