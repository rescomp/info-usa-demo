import argparse
import pandas as pd
import geopandas as gpd
import os
import warnings
warnings.filterwarnings('ignore')

def merge_files_spatial(spatial_file, zipcode_file, infousa_path, output_path):
    """
    Takes files the spatial file and determines which zipcodes lie within the geometries in the spatial file. Then,
    it writes these zipcodes into a textfile with one zipcode per line and outputs it into the output directory.
    Args:
        spatial_file (str): the complete path to the spatial file by which to subset
        ouzipcode_filetput_dir (str): a complete path to a shapefile containing geometries for all US zipcodes
        infousa_path (str): a complete path to the InfoUSA directory
        output_path (str): a complete path to a folder (with writing permissions) for the merged files to be written into
    """
        
    infousa_path = infousa_path
    spatial_path = spatial_file
    zipcode_path = zipcode_file 
    output_path = output_path
        
    # checking if these files exist
    if not os.path.exists(infousa_path):
        print("The InfoUSA directory path you inputted does not exist.")
        return
    if not os.path.exists(zipcode_path):
        print("The zipcode file path you inputted does not exist.")
        return
    if not os.path.exists(spatial_file):
        print("The spatial file path you inputted does not exist.")
        return
    if type(spatial_path) != str:
        print("Please input the directory path as a string")
        return
    
    # loading dataframes
    df_zipcodes = gpd.read_file(str(zipcode_path))
    df_spatial = gpd.read_file(spatial_path)
    
    # formatting zipcode dataframe
    df_zipcodes = df_zipcodes[['ZCTA5CE20', 'geometry']]
    df_zipcodes.rename(columns = {'ZCTA5CE20': 'zipcode'}, inplace = True)
    df_zipcodes['zipcode'] = df_zipcodes['zipcode'].astype('str')
    
    # first find intersection between dataframes
    df_intersect = gpd.sjoin(df_zipcodes, df_spatial, how = "inner", predicate = "intersects")
    
    df_intersect = df_intersect.drop_duplicates(subset = 'zipcode')
    df_intersect = df_intersect[['zipcode']]

    # create a list of the zipcodes in the interesection dataframe
    zipcode_list = df_intersect.zipcode.values.tolist()
    
    # make the list of zipcodes into a textfile, where each line
    # is a zipcode
    with open(str(output_path) + r'/zipcodes_to_merge.txt', 'w') as f:
        for zipcode in zipcode_list:
            f.write(f"{zipcode}\n")

def get_args_parse():
    
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Options for InfoUSA dataset subsetting')

    # Add the arguments
    my_parser.add_argument('--infousa', type=str, required=True, 
                           help='a complete path to the InfoUSA dataset')
    
    my_parser.add_argument('--output', type=str, required=True, 
                           help='a complete path to the folder you\'d like your output to be written to')

    my_parser.add_argument('--spatial', type=str, required=False,
                           help='a complete path to a .shp file with the geometries to subset by')
    
    my_parser.add_argument('--zipcode', type=str, required=False,
                           help='a complete path to a .shp file with zipcode geometries')
    
    # Execute the parse_args() method
    args = my_parser.parse_args()
    
    return args
if __name__ == '__main__':
    
    args = get_args_parse()
          
    if (not args.spatial) or (not args.zipcode):
        exit() # don't do anything in this case
        
    if args.spatial:
        spatial_file = get_args_parse().spatial
        zipcode_file = get_args_parse().zipcode
        infousa_path = get_args_parse().infousa
        output_path = get_args_parse().output
                
        merge_files_spatial(spatial_file, zipcode_file, infousa_path, output_path)
        
    else:
        print('No spatial file path entered')