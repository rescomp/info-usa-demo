#!/bin/bash

#SBATCH --job-name=check_folder.sh     ## Name of the job
#SBATCH --output=check_folder.out      ## Output file
#SBATCH --error=check_folder.err       ## Error file
#SBATCH --ntasks=1                     ## Number of tasks (analyses) to run
#SBATCH --cpus-per-task=1              ## The number of threads the code will use
#SBATCH --mem-per-cpu=50MB             ## Real memory(MB) per CPU required by the job. THIS IS THE PERFECT AMOUNT FOR 1000 FILES I CHECKED

source activate borsuklab

while :
do
    if [ ! -z "$(ls /hpc/group/borsuklab/infousa_processing/json_files)" ]; then
        echo "not empty!" >> check_folder.out
        json="/hpc/group/borsuklab/infousa_processing/json_files/*"       
        
        for file in $json; do
            json_name=$file
            sbatch --export=ALL,json_name=$json_name parse_json.sh
            echo "Submitted parse_json with: " $json_name >> check_folder.out
            sleep 30
        done
    fi
    sleep 90
done