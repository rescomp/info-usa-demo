#!/bin/bash

#SBATCH --job-name=parse_json          ## Name of the job
#SBATCH --output=parse_json.out        ## Output file
#SBATCH --error=parse_json.err         ## Error file
#SBATCH --ntasks=1                     ## Number of tasks (analyses) to run
#SBATCH --cpus-per-task=1              ## The number of threads the code will use
#SBATCH --mem-per-cpu=50MB             ## Real memory(MB) per CPU required by the job. THIS IS THE PERFECT AMOUNT FOR 1000 FILES I CHECKED

# source activate borsuklab

python parse_json.py --json $json_name

# sleep 2

# jobid=$(cat /hpc/home/at341/ondemand/infousa-processing-scripts/test_files/args.txt | sed -n '1p')
# netid=$(cat /hpc/home/at341/ondemand/infousa-processing-scripts/test_files/args.txt | sed -n '2p')
# zipcode_in=$(cat /hpc/home/at341/ondemand/infousa-processing-scripts/test_files/args.txt | sed -n '3p')
# spatial_in=$(cat /hpc/home/at341/ondemand/infousa-processing-scripts/test_files/args.txt | sed -n '4p')
# output_in=$(cat /hpc/home/at341/ondemand/infousa-processing-scripts/test_files/args.txt | sed -n '5p')
# output_filename=$(cat /hpc/home/at341/ondemand/infousa-processing-scripts/test_files/args.txt | sed -n '6p')

# zipcode_path_in='/hpc/group/borsuklab/at341/celine_data/source_files/zipcode_shapefiles/zipcodes.shp'
# infousa_in="/cifs/infousa"
# years='/hpc/home/at341/ondemand/infousa-processing-scripts/test_files/years.txt'

# sbatch --export=ALL,zipcode_in=$zipcode_in,infousa_in=$infousa_in,output_in=$output_in,spatial_in=$spatial_in,zipcode_path_in=$zipcode_path_in,output_filename=$output_filename,years=$years year_subset.sh