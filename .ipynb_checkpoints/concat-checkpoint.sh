#!/bin/bash

#SBATCH --job-name=final_batch_merge               ## Name of the job
#SBATCH --output=concat.out              ## Output file
#SBATCH --error=concat.err               ## Error file
#SBATCH --ntasks=1                    ## Number of tasks (analyses) to run
#SBATCH --cpus-per-task=10            ## The number of threads the code will use
#SBATCH --mem-per-cpu=2GB          ## Real memory(MB) per CPU required by the job. 20CPUs 10GB for all files

# source activate borsuklab
echo "output_filename is" $output_filename >> concat.out
python concat.py --input_dir $output_in --output_dir $output_in --output_filename $output_filename --year $year --jobid $jobid --netid $netid

echo "concat done." >> concat.out
