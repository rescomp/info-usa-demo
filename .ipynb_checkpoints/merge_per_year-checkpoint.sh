#!/bin/bash

#SBATCH --job-name=merge_per_year               ## Name of the job
#SBATCH --output=merge_per_year.out              ## Output file
#SBATCH --error=merge_per_year.err               ## Error file
#SBATCH --time=20:00                  ## Job Duration
#SBATCH --ntasks=1                    ## Number of tasks (analyses) to run
#SBATCH --cpus-per-task=4            ## The number of threads the code will use
#SBATCH --mem-per-cpu=5000MB          ## Real memory(MB) per CPU required by the job. THIS IS THE PERFECT AMOUNT FOR 1000 FILES I CHECKED


echo "infousa_in" $infousa_in >> merge_per_year.out

if [ $spatial_in != 'NA' ];
then 
    python spatial_subset.py --spatial $spatial_in --zipcode $zipcode_path_in --infousa $infousa_in --output $output_in
    ending_1="/zipcodes_to_merge/"
    ending_2=".txt"
    zipcode_in=$output_in$ending_1$year$ending_2
    echo "Did spatial subsetting." >> merge_per_year.out
fi
echo "Spatial subsetting complete" >> merge_per_year.out

sleep 2

# Split the file with the list of zipcodes to merge into 1000 zipcode per file chunks. 
echo "Running job to split input zipcode file." >> merge_per_year.out
python split.py --zipcode_input $zipcode_in --output_path $output_in --year $year --jobid $jobid --netid $netid
echo "Split complete." >> merge_per_year.out

# Then, for each file (split above), submit an sbatch job merging those 1000 files.
ending3="/split_files_"
hifen="-"
ending4="/*"
files=$output_in$ending3$year$hifen$jobid$hifen$netid$ending4

# After the textfile is split, for each smaller textfile, run merge.py to merge the 
# InfoUSA files with the zipcodes in that smaller textfile
echo "Running job to submit sbatch jobs, running merge.py with each 1000-zipcode file." >> merge_per_year.out

for file in $files; do
    file_name=$file
    sbatch --export=ALL,file_name=$file_name,infousa_in=$infousa_in,output_in=$output_in,filename=$file_name,year=$year,jobid=$jobid,netid=$netid batch_merge.sh
    
done
echo "Merge complete." >> merge_per_year.out

sleep 2

# Finally, concatenate the output files into one, final file. Save that into the output directory specified.
echo "Running job to merge all the output merged files into a final file." >> merge_per_year.out
sbatch --dependency=singleton --job-name=batch_merge --export=ALL,output_in=$output_in,output_filename=$output_filename,year=$year,jobid=$jobid,netid=$netid concat.sh
echo "Final merge complete." >> merge_per_year.out