import os
import argparse
import re
import shutil

def get_args_parse():
    
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Options for InfoUSA dataset subsetting')

    # Add the arguments

    my_parser.add_argument('--output_path', type=str, required=True, 
                           help='a complete path to the folder you\'d like your output to be written to')

    my_parser.add_argument('--zipcode_input', type=str, required=False, 
                           help='a complete path to a .txt file with the zipcodes to subset by')
    
    my_parser.add_argument('--year', type=str, required=False, 
                       help='year to pull data from')
    
    my_parser.add_argument('--jobid', type=str, required=False, 
                       help='JobID')
    
    my_parser.add_argument('--netid', type=str, required=False, 
                   help='User NetID')
    
    # Execute the parse_args() method
    args = my_parser.parse_args()
    
    return args

# want to make it so that it splits the file into files with only 1000 zipcodes each
if __name__ == '__main__':
    
    args = get_args_parse()
       
    zipcode_file = get_args_parse().zipcode_input
    output_path = get_args_parse().output_path
    year = get_args_parse().year
    jobid = get_args_parse().jobid
    netid = get_args_parse().netid

    # split zipcode input into 1000 file chunks. each chunk will be its own .txt file, starting with the
    # ending "zicpodes_new00.txt" onwards.
    # -l 1000: each chunk should have 1000 lines in it
    # -d: new files should be named 00, 01, 02, etc. instead of aa, ab, ac, etc.
    # os.system("split -l 1000 -d --additional-suffix=.txt '" + str(get_args_parse().zipcode_input) + "\' zipcodes_new")
    os.system("split -l 1000 -d --additional-suffix=.txt '" + str(get_args_parse().zipcode_input) + "\' zipcodes_" + str(year) + "-" + str(jobid) + "-" + str(netid) + "_")

    src_folder = os.getcwd()
    
    # create a new folder in the output directory to move these files to 
    os.mkdir(str(output_path) + '/split_files_' + str(year) + "-" + str(jobid) + "-" + str(netid))
    dst_folder = str(output_path) + '/split_files_' + str(year)  + "-" + str(jobid) + "-" + str(netid)
    # print(dst_folder)

    # make a list of all the files in the current working directory
    files_list = os.listdir(src_folder)
    
    # filter for only files containing the string zipcodes_new 
    r = re.compile('^zipcodes_' + str(year)  + "-" + str(jobid) + "-" + str(netid) + "_")
    files_to_move = list(filter(r.match, files_list))
    
    # move these to the directory just created
    for file in files_to_move:
        shutil.move(file, dst_folder + r'/' + file) 