import os
import argparse
import pandas as pd
import shutil
import ntpath
import zipfile

def get_args_parse():
    
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Options for InfoUSA dataset subsetting')

    # Add the arguments
    my_parser.add_argument('--json', type=str, required=True, 
                           help='a complete path to the json file to parse into arguments')
    
    # Execute the parse_args() method
    args = my_parser.parse_args()
    
    return args

def parse_arguments(json_file):
    json = "/hpc/group/borsuklab/infousa_processing/processed_json_files/" + str(json_file)

    df = pd.read_json(json)
    jobid = str(df['JobID'][0])
    netid = str(df['NetID'][0])
    zipcode_in = str(df['zipcode_in'][0])
    spatial_in = str(df['spatial_in'][0])
    output_in = str(df['output_in'][0])
    output_filename = str(df['output_filename'][0])
    
    years = df['years'].tolist()
    
    # REPLACE WITH PATH TO ZIPFILE
    # with zipfile.ZipFile(path_to_zip_file, 'r') as zip_ref:
    #     zip_ref.extractall("/hpc/group/borsuklab/infousa_processing/query_data_" + jobid) # make a different folder for each job so you know which to delete

    
    for year in years:
        os.system("sbatch --export=ALL,zipcode_in=" + zipcode_in + ",infousa_in=/cifs/infousa,output_in=" + output_in + ",spatial_in=" + spatial_in + ",zipcode_path_in=/hpc/group/borsuklab/at341/celine_data/source_files/zipcode_shapefiles/zipcodes.shp,output_filename=" + output_filename + ",year=" + str(year) + ",jobid=" + jobid + ",netid=" + netid + " merge_per_year.sh")

if __name__ == '__main__':
           
    json = get_args_parse().json
    
    # create a new folder in the output directory to move these files to 
    if not os.path.isdir("/hpc/group/borsuklab/infousa_processing/json_files"):
        os.mkdir("/hpc/group/borsuklab/infousa_processing/json_files")
        
    shutil.move(json, "/hpc/group/borsuklab/infousa_processing/processed_json_files")
    
    json = ntpath.basename(json)
    
    print(json)

    parse_arguments(json)
    
    
    